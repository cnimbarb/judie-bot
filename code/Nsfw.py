import discord
from discord import Embed, File
from discord.ext import commands, tasks
from discord.ext.commands import cooldown, BucketType
import random
from Utilities import HelperClass
from CharacterCard import NsfwCharacterCard


class Nsfw(commands.Cog):
    def __init__(self, client):
        self.client = client

    # Quotes for all OiaLt characters!
    Aiko = NsfwCharacterCard(name="Aiko",
                             picNumber=9,
                             quotes=["Don't just stand there, you're going to catch a cold!", "At least we'll have a funny anecdote to tell!"],
                             footers=["Why is he still covering my mouth?", "Man, if I die this sunday, this is what I hope heaven looks like."],
                             game="OiaLt")

    Carla = NsfwCharacterCard(name="Carla",
                              picNumber=15,
                              quotes=["Well, am I going to take a bath alone or..."],
                              footers=["haha stop it, we're not gonna fuck in the dressing room. We're not animals..."],
                              game="OiaLt")

    Iris = NsfwCharacterCard(name="Iris",
                             picNumber=14,
                             quotes=["Come on babe! Don't be shy! This is fucking great!", "Do you know what would be absolutely crazy and impulsive right now?"],
                             footers=["*hint: not stopping at a BJ!*"],
                             game="OiaLt")

    Jasmine = NsfwCharacterCard(name="Jasmine",
                                picNumber=9,
                                quotes=["You fucking prick... I don't know why, but you really turn me on...",
                                        "You're precious and you're smokin' hot, it had to happen sooner or later..."],
                                footers=["Well, your ass looks good in these, I'll give you that.",
                                         "I'll have to stand aside, [...] let's say that MC really took it out of me..."],
                                game="OiaLt")

    Judie = NsfwCharacterCard(name="Judie",
                              picNumber=27,
                              quotes=["This was such a good idea, sis!", "Come on babe! Don't be shy! This is fucking great!",
                                      "I don't know if I can take it step-bro!", "Hmm, this turns me on so much..."],
                              footers=["You're setting the bar so high though...", "Duh, we had sex in Japan too!",
                                       "I wanna know what you taste like..."],
                              game="OiaLt")

    Lauren = NsfwCharacterCard(name="Lauren",
                               picNumber=25,
                               quotes=["You're precious and you're smokin' hot, it had to happen sooner or later...",
                                       "At least we'll have a funny anecdote to tell!", "This was such a good idea, sis!",
                                       "These are the ladies' baths, you shouldn't be here...",
                                       "Are you ready for the best massage your back will ever have?"],
                               footers=["Man, if I die this sunday, this is what I hope heaven looks like.",
                                        "I'll have to stand aside, [...] let's say that MC really took it out of me...",
                                        "You're setting the bar so high though...",
                                        "Good! Then we can chill! See how easy that was?",
                                        "I see you've made yourself comfortable"],
                               game="OiaLt")

    Rebecca = NsfwCharacterCard(name="Rebecca",
                                picNumber=10,
                                quotes=["You fill me up completely"],
                                footers=["I'm not a respectable teacher now, am I?"],
                                game="OiaLt")

    Alex = NsfwCharacterCard(name="Alex",
                             picNumber=4,
                             quotes=["Oh you didn't know? Orion and I are fucking. Like rabbits."],
                             footers=["You wouldn't want my daddy to hear us, would you...?"],
                             game="Eternum")

    Annie = NsfwCharacterCard(name="Annie",
                              picNumber=8,
                              quotes=["\"Like\" it? It was the best experience of my life!"],
                              footers=["My knight in shining armor..."],
                              game="Eternum")

    Calypso = NsfwCharacterCard(name="Calypso",
                                picNumber=4,
                                quotes=["You look... \"hot as fuck\" as well."],
                                footers=["Holy Mother of Turska!"],
                                game="Eternum")

    Dalia = NsfwCharacterCard(name="Dalia",
                              picNumber=8,
                              quotes=["\*Panting\* T-Too much... d-dick... N-Need... oxygen!"],
                              footers=["*cum in my mouth.*"],
                              game="Eternum")

    EvaTroll = NsfwCharacterCard(name="SnoopWho",
                                 picNumber=1,
                                 quotes=["Eva who?"],
                                 footers=["side dish?"],
                                 game="Eternum")

    EvaSerious = NsfwCharacterCard(name="Eva",
                                   picNumber=5,
                                   quotes=["I do find Mr. Orion very intriguing... and I'd love to learn more about him..."],
                                   footers=["Let me help you relax and ease your mind..."],
                                   game="Eternum")

    FoxMaidens = NsfwCharacterCard(name="FoxMaidens",
                                   picNumber=3,
                                   quotes=[""],
                                   footers=[""],
                                   game="Eternum")

    # Quotes!
    Luna = NsfwCharacterCard(name="Luna",
                             picNumber=6,
                             quotes=["Thank you for everything you said. It was... sweet."],
                             footers=["It's amazing!"],
                             game="Eternum")

    Maat = NsfwCharacterCard(name="Maat",
                             picNumber=11,
                             quotes=["I want you to destroy me..."],
                             footers=["This spectacle is pleasing the pharaohs!"],
                             game="Eternum")

    # Quotes!
    Nancy = NsfwCharacterCard(name="Nancy",
                              picNumber=24,
                              quotes=["Christ!! I'm looking like some inexperienced teenager here..."],
                              footers=["Man, blue-balled by fucking war."],
                              game="Eternum")

    Nova = NsfwCharacterCard(name="Nova",
                             picNumber=13,
                             quotes=["Have you ever had sex with a girl? Is she still alive?"],
                             footers=["Satisfying the wishes of a nympho freak seems like a small price to pay."],
                             game="Eternum")

    Penny = NsfwCharacterCard(name="Penny",
                              picNumber=9,
                              quotes=["How should we do it?"],
                              footers=["Thanks for supporting me..."],
                              game="Eternum")

    Wenlin = NsfwCharacterCard(name="Wenlin",
                               picNumber=11,
                               quotes=[""],
                               footers=[""],
                               game="Eternum")

    Eva = []
    Eva.append(EvaTroll)
    Eva.append(EvaSerious)

    oialt = []
    oialt.append(Aiko)
    oialt.append(Carla)
    oialt.append(Iris)
    oialt.append(Jasmine)
    oialt.append(Judie)
    oialt.append(Lauren)
    oialt.append(Rebecca)

    options = []
    options.append(Aiko)
    options.append(Carla)
    options.append(Iris)
    options.append(Jasmine)
    options.append(Judie)
    options.append(Lauren)
    options.append(Rebecca)
    options.append(Alex)
    options.append(Annie)
    options.append(Calypso)
    options.append(Dalia)
    options.append(EvaSerious)
    options.append(FoxMaidens)
    options.append(Luna)
    options.append(Maat)
    options.append(Nancy)
    options.append(Nova)
    options.append(Penny)
    options.append(Wenlin)

    eternum = []
    eternum.append(Alex)
    eternum.append(Annie)
    eternum.append(Calypso)
    eternum.append(Dalia)
    eternum.append(EvaSerious)
    eternum.append(FoxMaidens)
    eternum.append(Luna)
    eternum.append(Maat)
    eternum.append(Nancy)
    eternum.append(Nova)
    eternum.append(Penny)
    eternum.append(Wenlin)

    @commands.command()
    async def nsfw(self, ctx, parameter=None):
        if ctx.channel.is_nsfw():
            choice = ""
            if parameter is None:
                choice = random.choice(self.options)
            elif parameter.lower() == "aiko":
                choice = self.Aiko
            elif parameter.lower() == "carla":
                choice = self.Carla
            elif parameter.lower() == "iris":
                choice = self.Iris
            elif parameter.lower() in ["jasmine", "jas"]:
                choice = self.Jasmine
            elif parameter.lower() == "judie":
                choice = self.Judie
            elif parameter.lower() == "lauren":
                choice = self.Lauren
            elif parameter.lower() in ["rebecca", "reb"]:
                choice = self.Rebecca
            elif parameter.lower() == "oialt" or parameter.lower() == "once in a lifetime":
                choice = random.choice(self.oialt)
            elif parameter.lower() == "alex" or parameter.lower() == "alexandra":
                choice = self.Alex
            elif parameter.lower() == "annie":
                choice = self.Annie
            elif parameter.lower() == 'calypso':
                choice = self.Calypso
            elif parameter.lower() == "dalia":
                choice = self.Dalia
            elif parameter.lower() == "eva":
                choice = random.choice(self.Eva)
            elif parameter.lower() in ['fox maidens', 'foxmaidens', 'fox girls']:
                choice = self.FoxMaidens
            elif parameter.lower() == "luna":
                choice = self.Luna
            elif parameter.lower() == "maat":
                choice = self.Maat
            elif parameter.lower() == "nancy":
                choice = self.Nancy
            elif parameter.lower() == "nova":
                choice = self.Nova
            elif parameter.lower() == "penny" or parameter.lower() == "penelope":
                choice = self.Penny
            elif parameter.lower == "wenlin":
                choice = self.Wenlin
            elif parameter.lower() == "eternum":
                choice = random.choice(self.eternum)
            else:
                choice = random.choice(self.options)

            color = HelperClass.orange
            if choice.game == "Eternum":
                color = HelperClass.eternumBlue

            embed = discord.Embed(title=choice.name, description=random.choice(choice.quotes), colour=color)
            embed.set_footer(text=random.choice(choice.footers))

            number = random.randint(1, choice.picNumber)
            image = discord.File(f'./NsfwPics/{choice.game}/{choice.name}/{choice.name}_{number}.png', filename=f"nsfw.png")
            embed.set_image(url=f"attachment://nsfw.png")
            await ctx.send(file=image, embed=embed)
        else:
            await ctx.send("This channel is sfw!")


def setup(client):
    client.add_cog(Nsfw(client))
