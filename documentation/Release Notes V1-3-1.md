## Changelog Version 1.3.1
**Uploaded 19. January 2022**

Fixed a critical bug

### Changes:

- Noticed a failure to update for users that had the priestess as their last collected potential LI. Fixed that issue

- Changed a few extra smol issues that are really just eye candy.
